use std::fs;

fn main() {
    let path = "elves.txt";

    let input = fs::read_to_string(path).unwrap();
    let mut totals = vec![0];

    for line in input.split("\n") {
        match line {
            "" => totals.push(0),

            value if value.parse::<i32>().is_ok() => {
                let mut current = totals.pop().unwrap();
                current += value.parse::<i32>().unwrap();
                totals.push(current);
            }

            _ => println!("Not able to parse an elves stuff"),
        };
    }

    totals.sort();
    totals.reverse();

    println!("Most calaries are: {:?}", totals[0]);
    println!(
        "The three most calaries are: {:?}",
        totals[0] + totals[1] + totals[2]
    );
}
