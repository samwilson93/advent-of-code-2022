use std::fs;

fn main() {
    let path = "elves.txt";

    let input = fs::read_to_string(path).unwrap();

    let mut totals: Vec<Option<u32>> = input
        .split("\n\n")
        .map(|elf| {
            elf.split("\n")
                .map(|str_value| str_value.parse::<u32>().unwrap())
                .reduce(|total, current| total + current)
        })
        .collect();
        
    totals.sort();
    totals.reverse();

    println!("Alternate most calaries are: {:?}", totals[0].unwrap());
    println!(
        "The three most calaries are: {:?}",
        totals[0].unwrap() + totals[1].unwrap() + totals[2].unwrap()
    );
}
